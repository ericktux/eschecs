
# Delete old configuration files

rm -f ./config/eschecs.ini
rm -f ./config/history.fen

# Use Linux engines

cp -f config/linux.ini config/engines.ini

# Delete old files

rm -f ./*.err
rm -f ./*.log
rm -f ./game.pgn

# Start Eschecs

# engine=engines/linux/cheese/311/cheese-311-linux
engine=engines/linux/stash/30/stash-bot

./eschecs \
$engine \
--openingbook='books/Perfect2021.bin' \
--time=500 \
2> eschecs.err

exit 0

./eschecs \
engines/cheng/441/cheng4_linux_x64 \
--openingbook='books/Perfect2021.bin' \
--time=200 \
2> eschecs.err

exit 0

./eschecs \
/home/roland/Documents/echecs/sources/ct800/142/source/application-uci/output/CT800_V1.42 \
--position='rnbqk1nr/1ppp1ppp/p3p3/8/1bB1P3/2N5/PPPP1PPP/R1BQK1NR w HAha - 2 4' \
--autoplay=true \
--upsidedown=false \
--chessboard=simple \
--time=500 \
--font=montreal \
--language=russian \
--size=60 \
--white=FFFF00FF \
--black=FFA500FF \
--green=60C00080 \
--red=C0000080 \
--volume=10 \
2> eschecs.err

exit 0

./eschecs \
/home/roland/Documents/echecs/sources/ct800/142/source/application-uci/output/CT800_V1.42 \
--position='rnbqk1nr/1ppp1ppp/p3p3/8/1bB1P3/2N5/PPPP1PPP/R1BQK1NR w HAha - 2 4' \
--autoplay=true \
--upsidedown=false \
--chessboard=marblecustom \
--marblecolors=FFFFFFFF,0080B3FF,0066FFFF,0047B3FF \
--time=500 \
--font=montreal \
--language=french \
--size=60 \
--green=60C00080 \
--red=C0000080 \
--volume=10 \
2> eschecs.err

exit 0

./eschecs \
/home/roland/Documents/echecs/sources/ct800/142/source/application-uci/output/CT800_V1.42 \
-p 'rnbqk1nr/1ppp1ppp/p3p3/8/1bB1P3/2N5/PPPP1PPP/R1BQK1NR w HAha - 2 4' \
-a true \
-u false \
-c marblecustom \
-m FFFFFFFF,0080B3FF,0066FFFF,0047B3FF \
-t 1001 \
-f montreal \
-l french \
-s 60 \
-g 60C00080 \
-r C0000080 \
-v 10 \
2> eschecs.err

exit 0

./eschecs \
/home/roland/Documents/echecs/sources/ct800/142/source/application-uci/output/CT800_V1.42 \
-p 'rnbqk1nr/1ppp1ppp/p3p3/8/1bB1P3/2N5/PPPP1PPP/R1BQK1NR w HAha - 2 4' \
-a true \
-u false \
-c marble \
-t 1001 \
-f montreal \
-l french \
-s 60 \
-g 60C00080 \
-r C0000080 \
-v 10 \
2> eschecs.err
