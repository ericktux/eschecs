
unit Settings;

interface

uses
  SysUtils, BGRABitmapTypes, Images, Language, Utils, Fen;

procedure LoadSettings(
  out APos: string;
  out AAutoPlay, AUpsideDown: boolean;
  out AStyle: TBoardStyle;
  out AHist: string;
  out APosIndex: integer;
  out AEngine: TFileName;
  out ABook: TFileName;
  out ALSColor, ADSColor, AGreen, ARed, ALMColor1, ALMColor2, ADMColor1, ADMColor2: TBGRAPixel;
  out AMoveTime: integer;
  out AFont: string;
  out ALang: TLanguage;
  out AScale: integer;
  out AChess960: boolean
);
procedure SaveSettings(
  const APos: string;
  const AAutoPlay, AUpsideDown: boolean;
  const AStyle: TBoardStyle;
  const AHist: string;
  const APosIndex: integer;
  const AEngine: TFileName;
  const ABook: TFileName;
  const ALSColor, ADSColor, AGreen, ARed, ALMColor1, ALMColor2, ADMColor1, ADMColor2: TBGRAPixel;
  const AMoveTime: integer;
  const AFont: string;
  const ALang: TLanguage;
  const AScale: integer;
  const AChess960: boolean
);

const
{$IFDEF WINDOWS}
{$IFDEF CPU64}
  CDefaultEngine = 'engines/cheng/440/cheng4_x64.exe';
{$ELSE}
  CDefaultEngine = 'engines/cheng/439/cheng4.exe';
{$ENDIF}
{$ELSE}
  CDefaultEngine = 'engines/cheng/440/cheng4_linux_x64';
{$ENDIF}
  CDefaultBook = 'books/gm2001.bin';
  CDefaultPosition: array[boolean] of string = (
    CFenStartPosition,
    CFenStartPosition518
  );
  
implementation

uses
  IniFiles;

const
  CSectionMain = 'main';
  CSectionColors = 'colors';
  CSectionEngine = 'engine';
  CSectionGame = 'game';
  CDefaultAutoplay = 'true';
  CDefaultUpsideDown = 'false';
  CDefaultStyle = bsSimple;  
  CDefaultHistory = '';
  CDefaultIndex = 0;
  CDefaultMoveTime = 1000;
  CDefaultFont = 'montreal';
  CDefaultLanguage = lgEnglish;
  CDefaultScale = 40;
  CDefaultChess960 = 'false';
  
var
  LIniFileName: TFileName;
  
procedure LoadSettings(
  out APos: string;
  out AAutoPlay, AUpsideDown: boolean;
  out AStyle: TBoardStyle;
  out AHist: string;
  out APosIndex: integer;
  out AEngine: TFileName;
  out ABook: TFileName;
  out ALSColor, ADSColor, AGreen, ARed, ALMColor1, ALMColor2, ADMColor1, ADMColor2: TBGRAPixel;
  out AMoveTime: integer;
  out AFont: string;
  out ALang: TLanguage;
  out AScale: integer;
  out AChess960: boolean
);
begin
  with TIniFile.Create(LIniFileName) do
  try
    AFont := ReadString(CSectionMain, 'font', CDefaultFont);
    AScale := ReadInteger(CSectionMain, 'scale', CDefaultScale);
    AStyle := TBoardStyle(ReadInteger(CSectionMain, 'style', Ord(CDefaultStyle)));
    AUpsideDown := LowerCase(ReadString(CSectionMain, 'upsidedown', CDefaultUpsideDown)) = 'true';
    ALang := TLanguage(ReadInteger(CSectionMain, 'language', Ord(CDefaultLanguage)));
    
    ALSColor := StrToBGRA(ReadString(CSectionColors, 'light', 'A9A9A9FF'));
    ADSColor := StrToBGRA(ReadString(CSectionColors, 'dark', '808080FF'));
    ALMColor1 := StrToBGRA(ReadString(CSectionColors, 'lightmarble1', 'EEEEEEFF'));
    ALMColor2 := StrToBGRA(ReadString(CSectionColors, 'lightmarble2', 'CCCCCCFF'));
    ADMColor1 := StrToBGRA(ReadString(CSectionColors, 'darkmarble1', '444444FF'));
    ADMColor2 := StrToBGRA(ReadString(CSectionColors, 'darkmarble2', '666666FF'));
    AGreen := StrToBGRA(ReadString(CSectionColors, 'green', '60C00080'));
    ARed := StrToBGRA(ReadString(CSectionColors, 'red', 'C0000080'));

    AEngine := ReadString(CSectionEngine, 'engine', CDefaultEngine);
    ABook := ReadString(CSectionEngine, 'book', CDefaultBook);
    AAutoPlay := LowerCase(ReadString(CSectionEngine, 'autoplay', CDefaultAutoplay)) = 'true';
    AMoveTime := ReadInteger(CSectionEngine, 'movetime', CDefaultMoveTime);
    
    AChess960 := LowerCase(ReadString(CSectionGame, 'chess960', CDefaultChess960)) = 'true';
    APos := ReadString(CSectionGame, 'position', CDefaultPosition[AChess960]);
    AHist := ReadString(CSectionGame, 'history', CDefaultHistory);
    APosIndex := ReadInteger(CSectionGame, 'index', CDefaultIndex);
  finally
    Free;
  end;
end;

procedure SaveSettings(
  const APos: string;
  const AAutoPlay, AUpsideDown: boolean;
  const AStyle: TBoardStyle;
  const AHist: string;
  const APosIndex: integer;
  const AEngine: TFileName;
  const ABook: TFileName;
  const ALSColor, ADSColor, AGreen, ARed, ALMColor1, ALMColor2, ADMColor1, ADMColor2: TBGRAPixel;
  const AMoveTime: integer;
  const AFont: string;
  const ALang: TLanguage;
  const AScale: integer;
  const AChess960: boolean
);
begin
  with TIniFile.Create(LIniFileName) do
  try
    WriteString(CSectionMain, 'font', AFont);
    WriteInteger(CSectionMain, 'scale', AScale);
    WriteInteger(CSectionMain, 'style', Ord(AStyle));
    WriteString(CSectionMain, 'upsidedown', LowerCase(BoolToStr(AUpsideDown, TRUE)));
    WriteInteger(CSectionMain, 'language', Ord(ALang));
    
    WriteString(CSectionColors, 'light', BGRAToStr(ALSColor));
    WriteString(CSectionColors, 'dark', BGRAToStr(ADSColor));
    WriteString(CSectionColors, 'lightmarble1', BGRAToStr(ALMColor1));
    WriteString(CSectionColors, 'lightmarble2', BGRAToStr(ALMColor2));
    WriteString(CSectionColors, 'darkmarble1', BGRAToStr(ADMColor1));
    WriteString(CSectionColors, 'darkmarble2', BGRAToStr(ADMColor2));
    WriteString(CSectionColors, 'green', BGRAToStr(AGreen));
    WriteString(CSectionColors, 'red', BGRAToStr(ARed));

    WriteString(CSectionEngine, 'engine', AEngine);
    WriteString(CSectionEngine, 'book', ABook);
    WriteString(CSectionEngine, 'autoplay', LowerCase(BoolToStr(AAutoPlay, TRUE)));
    WriteInteger(CSectionEngine, 'movetime', AMoveTime);
    
    WriteString(CSectionGame, 'position', APos);
    WriteString(CSectionGame, 'chess960', LowerCase(BoolToStr(AChess960, TRUE)));
    WriteString(CSectionGame, 'history', AHist);
    WriteInteger(CSectionGame, 'index', APosIndex);
    
    UpdateFile;
  finally
    Free;
  end;
end;

begin
  LIniFileName := Concat(LConfigFilesPath, 'eschecs.ini');
end.
