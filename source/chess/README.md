# Pascal Chess Library

Pascal units for management of a chess game.

Can read FEN and PGN format. Supports Fischer Random Chess.

## Example

```pascal
uses
  ChessGame, Fen;

begin
  with TChessGame.Create(CFenStartPosition) do
  try
    if IsLegal('e2e4') then
      DoMove('e2e4');
  finally
    Free;
  end;
end.
```

## How to use

All demo programs can be compiled and executed from the *Makefile*.

To build the demo, open a console/terminal and type
```
make
```
