
uses
  SysUtils, Classes, Crt, ChessTypes, Chessboard, FenFilter, FenExamples;

type
  TErrorLevel = (NoError, StringFormatError, PositionError);
  
function CheckPosition(const APos: string): TErrorLevel;
var
  f: TFenFilter;
  p: TChessPosition;
begin
  f := TFenFilter.Create;
  if f.IsFen(APos) then
  begin
    p := TChessPosition.Create(APos);
    if p.FENRecord(FALSE) = APos then
      result := NoError
    else
      result := PositionError;
    p.Free;
  end else
    result := StringFormatError;
  f.Free;
end;

const
  CColors: array[TErrorLevel] of byte = (LightGray, LightRed, LightBlue);
var
  i: integer;
  e: TErrorLevel;
begin
  for i := Low(CFenDestructionExamples) to High(CFenDestructionExamples) do
  begin
    e := CheckPosition(CFenDestructionExamples[i]);
    TextColor(CColors[e]);
    WriteLn(CFenDestructionExamples[i]);
  end;
  TextColor(LightGray);
end.
