
unit MainForm;
{.$DEFINE USE_SCRIPT}
interface

uses
  SysUtils,
  Classes,
  Process,
  TypInfo,
  
  fpg_base,
  fpg_main,
  fpg_menu,
  fpg_widget,
  fpg_form,
  fpg_label,
  fpg_edit ,
  fpg_button,
  fpg_combobox,
  fpg_dialogs,
  fpg_checkbox,
  
  BGRABitmapTypes,
  
  NaturalSort,
  Language,
  Images,
  Settings,
  Constants;

type
  TMainForm = class(TfpgForm)
  private
    FMenuBar: TfpgMenuBar;
    FAssistantSubMenu: TfpgPopupMenu;
    LBFont: TfpgLabel;
    CBFont: TfpgComboBox;
    LBSize: TfpgLabel;
    CBSize: TfpgComboBox;
    CBUpsideDown: TfpgCheckBox;
    LBStyle: TfpgLabel;
    CBStyle: TfpgComboBox;
    LBLang: TfpgLabel;
    CBLang: TfpgComboBox;
    LBLSColor: TfpgLabel;
    EDLSColor: TfpgEdit;
    LBDSColor: TfpgLabel;
    EDDSColor: TfpgEdit;
    LBGreen: TfpgLabel;
    EDGreen: TfpgEdit;
    LBRed: TfpgLabel;
    EDRed: TfpgEdit;
    LBLMColor1: TfpgLabel;
    EDLMColor1: TfpgEdit;
    LBLMColor2: TfpgLabel;
    EDLMColor2: TfpgEdit;
    LBDMColor1: TfpgLabel;
    EDDMColor1: TfpgEdit;
    LBDMColor2: TfpgLabel;
    EDDMColor2: TfpgEdit;
    LBEngine: TfpgLabel;
    EDEngine: TfpgEdit;
    LBTime: TfpgLabel;
    EDTime: TfpgEdit;
    CBAutoPlay: TfpgCheckBox;
    BTSelectEngine: TfpgButton;
    LBBook: TfpgLabel;
    EDBook: TfpgEdit;
    BTSelectBook: TfpgButton;
    LBPos: TfpgLabel;
    EDPos: TfpgEdit;
    BTStart: TfpgButton;
    BTQuit: TfpgButton;
    procedure CBFontChange(Sender: TObject);
    procedure BTSelectEngineClick(Sender: TObject);
    procedure BTSelectBookClick(Sender: TObject);
    procedure BTStartClick(Sender: TObject);
    procedure BTQuitClick(Sender: TObject);
    procedure ItemQuitClicked(Sender: TObject);
    procedure PopulateFont;
    procedure PopulateSize;
    procedure PopulateLang;
    procedure PopulateStyle;
  public
    procedure AfterCreate; override;
  end;

implementation

var
  LCurrPos: string;
  LAuto, LUpsideDown: boolean;
  LStyle: TBoardStyle;
  LMoveHist: string;
  LPosIndex: integer;
  LEngine, LBook: TFileName;
  LLSColor, LDSColor, LGreen, LRed, LLMColor1, LLMColor2, LDMColor1, LDMColor2: TBGRAPixel;
  LMoveTime: integer;
  LFont: string;
  LLang: TLanguage;
  LLangStr: string;
  LScale: integer;
  LChess960: boolean;
  
function Capitalize(const AStr: string): string;
begin
  if Length(AStr) = 0 then
    result := ''
  else
    result := Concat(
      UpCase(AStr[1]),
      Copy(AStr, 2)
    );
end;

procedure TMainForm.CBFontChange(Sender: TObject);
var
  i: integer;
begin
  PopulateSize;
  for i := 0 to Pred(CBSize.Items.Count) do
    if CBSize.Items[i] = IntToStr(LScale) then
      CBSize.FocusItem := i;
end;

procedure TMainForm.BTSelectEngineClick(Sender: TObject);
var
  LDialog: TfpgFileDialog;
begin
  LDialog := TfpgFileDialog.Create(self);
  try
    LDialog.Filter := rsUciChessEngine + {$IFDEF WINDOWS}' (*.exe)|*.exe'{$ELSE}' (*)|*'{$ENDIF};
    if LDialog.RunOpenFile then
      EDEngine.Text := LDialog.FileName;
  finally
    LDialog.Free;
  end;
end;

procedure TMainForm.BTSelectBookClick(Sender: TObject);
var
  LDialog: TfpgFileDialog;
begin
  LDialog := TfpgFileDialog.Create(self);
  try
    LDialog.Filter := rsPolyglotBook + ' (*.bin)|*.bin';
    if LDialog.RunOpenFile then
      EDBook.Text := LDialog.FileName;
  finally
    LDialog.Free;
  end;
end;

procedure TMainForm.BTStartClick (Sender: TObject );
const
  CBoolStr: array[boolean] of string = ('false', 'true');
  CLineBreak = ' \';
  CScriptName = 'start.sh';
var
  LProcess: TProcess;
  LScript: TStringList;
  LStyleStr: string;
begin
  LFont := LowerCase(CBFont.Text);
  LLang := TLanguage(CBLang.FocusItem);
  LLangStr := LowerCase(Copy(GetEnumName(TypeInfo(TLanguage), Ord(LLang)), 3));
  LMoveTime := StrToIntDef(EDTime.Text, 500);
  LScale := StrToIntDef(CBSize.Text, 50);
  LStyle := TBoardStyle(CBStyle.FocusItem);
  LStyleStr := LowerCase(CBStyle.Text); 
  LCurrPos := EDPos.Text;
  
  if (LStyle = bsWood) and (LScale > 80) then
  begin
    ShowMessage(rsWoodChessboardNotAvailable);
    Exit;
  end;
  
  LProcess := TProcess.Create(nil);
{$IFDEF USE_SCRIPT}
  LScript := TStringList.Create;
  LScript.Append('./eschecs' + CLineBreak);
  LScript.Append(EDEngine.Text + CLineBreak);
  LScript.Append(Format('--time=%d' + CLineBreak, [LMoveTime]));
  LScript.Append(Format('--autoplay=%s' + CLineBreak, [CBoolStr[CBAutoPlay.Checked]]));
  LScript.Append(Format('--position="%s"' + CLineBreak, [LCurrPos]));
  LScript.Append(Format('--font=%s' + CLineBreak, [LFont]));
  LScript.Append(Format('--size=%d' + CLineBreak, [LScale]));
  LScript.Append(Format('--chessboard=%s' + CLineBreak, [LStyleStr]));
  LScript.Append(Format('--upsidedown=%s' + CLineBreak, [CBoolStr[CBUpsideDown.Checked]]));
  LScript.Append(Format('--language=%s', [LLangStr]));
  LScript.SaveToFile(CScriptName);
  LScript.Free;
  
  LProcess.Executable := '/bin/sh';
  LProcess.Parameters.Add(CScriptName);
{$ELSE}
  LProcess.Executable := {$IFDEF WINDOWS}'eschecs.exe'{$ELSE}'eschecs'{$ENDIF};
  LProcess.Parameters.Add(EDEngine.Text);
  LProcess.Parameters.Add(Format('--openingbook=%s', [EDBook.Text]));
  LProcess.Parameters.Add(Format('--time=%d', [LMoveTime]));
  LProcess.Parameters.Add(Format('--autoplay=%s', [CBoolStr[CBAutoPlay.Checked]]));
  LProcess.Parameters.Add(Format('--position=%s', [LCurrPos]));
  LProcess.Parameters.Add(Format('--font=%s', [LFont]));
  LProcess.Parameters.Add(Format('--size=%d', [LScale]));
  LProcess.Parameters.Add(Format('--chessboard=%s', [LStyleStr]));
  LProcess.Parameters.Add(Format('--upsidedown=%s', [CBoolStr[CBUpsideDown.Checked]]));
  LProcess.Parameters.Add(Format('--language=%s', [LLangStr]));
{$ENDIF}
  LProcess.Execute;
  LProcess.Free;
  
  Close;
end;

procedure TMainForm.BTQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TMainForm.PopulateFont;
var
  LRec: TSearchRec;
  LList: TStringList;
  LIndex: integer;
begin
  CBFont.Items.Clear;
  LList := TStringList.Create;
  LList.Sorted := TRUE;
  if FindFirst(
    Concat(ExtractFilePath(ParamStr(0)), 'images/pieces/*'),
    faAnyFile or faDirectory,
    LRec
  ) = 0 then
  repeat
    with LRec do
      if ((Attr and faDirectory) = faDirectory) and (Name <> '.') and (Name <> '..') then
        LList.Append(Capitalize(Name));
  until FindNext(LRec) <> 0;
  FindClose(LRec);
  for LIndex := 0 to LList.Count - 1 do
    CBFont.Items.Add(LLIst[LIndex]);
  LList.Free;
end;

procedure TMainForm.PopulateSize;
var
  LRec: TSearchRec;
  LList: TStringList;
  LIndex: integer;
begin
  CBSize.Items.Clear;
  LList := TStringList.Create;
  if FindFirst(
    Concat(ExtractFilePath(ParamStr(0)), 'images/pieces/', LowerCase(CBFont.Text), '/*'),
    faAnyFile or faDirectory,
    LRec
  ) = 0 then
  repeat
    with LRec do
      if ((Attr and faDirectory) = faDirectory) and (Name <> '.') and (Name <> '..') then
        LList.Append(Name);
  until FindNext(LRec) <> 0;
  FindClose(LRec);
  Sort(LList, stNatural);
  for LIndex := 0 to LList.Count - 1 do
    CBSize.Items.Add(LLIst[LIndex]);
  LList.Free;
end;

procedure TMainForm.PopulateLang;
var
  LLang: TLanguage;
  LName: string;
begin
  CBLang.Items.Clear;
  for LLang := Low(TLanguage) to High(TLanguage) do
  begin
    LName := Copy(GetEnumName(TypeInfo(TLanguage), Ord(LLang)), 3);
    CBLang.Items.Add(LName);
  end;
end;

procedure TMainForm.PopulateStyle;
var
  LStyle: TBoardStyle;
  LName: string;
begin
  CBStyle.Items.Clear;
  for LStyle := Low(TBoardStyle) to High(TBoardStyle) do
  begin
    LName := Copy(GetEnumName(TypeInfo(TBoardStyle), Ord(LStyle)), 3);
    CBStyle.Items.Add(LName);
  end;
end;

procedure TMainForm.AfterCreate;
var
  i: integer;
begin
  Name := 'MainForm';
  SetPosition(300, 200, 504, 516);
  WindowTitle := 'Assistant';
  Hint := '';
  IconName := 'vfd.eschecs';
  WindowPosition := wpOneThirdDown;
  MinWidth := 504;
  MinHeight := 516;
  
{$IFDEF WINDOWS}
  Sizeable := FALSE;
{$ENDIF}
  
  FMenuBar := TfpgMenuBar.Create(self);
  with FMenuBar do
  begin
    Name := 'FMenuBar';
    Align := alTop;
    SetPosition(0, 0, 504, 24);
    Anchors := [anLeft, anRight, anTop];
  end;
  FAssistantSubMenu := TfpgPopupMenu.Create(self);
  with FAssistantSubMenu do Name := 'FAssistantSubMenu';
  
  FMenuBar.AddMenuItem('Assistant', nil).SubMenu := FAssistantSubMenu;
  FAssistantSubMenu.AddMenuItem(rsQuit, 'Ctrl+Q', @ItemQuitClicked);
  
  LBFont := TfpgLabel.Create(self);
  with LBFont do
  begin
    Name := 'LBFont';
    SetPosition(8, 32, 240, 16);
    FontDesc := '#Label1';
    Hint := '';
    Text := rsPiecesSet;
  end;
  
  CBFont := TfpgComboBox.Create(self);
  with CBFont do
  begin
    Name := 'CBFont';
    SetPosition(8, 50, 240, 22);
    FontDesc := '#List';
    Hint := '';
    TabOrder := 0;
    OnChange := @CBFontChange;
  end;

  LBSize := TfpgLabel.Create(self);
  with LBSize do
  begin
    Name := 'LBSize';
    SetPosition(256, 32, 116, 16);
    FontDesc := '#Label1';
    Text := rsPiecesSize;
  end;
  
  CBSize := TfpgComboBox.Create(self);
  with CBSize do
  begin
    Name := 'CBSize';
    SetPosition(256, 50, 116, 22);
    FontDesc := '#List';
    TabOrder := 1;
  end;
  
  CBUpsideDown := TfpgCheckBox.Create(self);
  with CBUpsideDown do
  begin
    Name := 'CBUpsideDown';
    SetPosition(380, 50, 116, 22);
    FontDesc := '#Label1';
    TabOrder := 2;
    Text := rsUpsideDown;
  end;
  
  LBStyle := TfpgLabel.Create(self);
  with LBStyle do
  begin
    Name := 'LBStyle';
    SetPosition(8, 80, 240, 16);
    FontDesc := '#Label1';
    Text := rsChessboardStyle;
  end;
  
  CBStyle := TfpgComboBox.Create(self);
  with CBStyle do
  begin
    Name := 'CBStyle';
    SetPosition(8, 98, 240, 22);
    FontDesc := '#List';
    TabOrder := 3;
  end;

  LBLang := TfpgLabel.Create(self);
  with LBLang do
  begin
    Name := 'LBLang';
    SetPosition(256, 80, 240, 16);
    FontDesc := '#Label1';
    Text := rsLanguage;
  end;
  
  CBLang := TfpgComboBox.Create(self);
  with CBLang do
  begin
    Name := 'CBLang';
    SetPosition(256, 98, 240, 22);
    FontDesc := '#List';
    TabOrder := 4;
  end;

  LBLSColor := TfpgLabel.Create(self);
  with LBLSColor do
  begin
    Name := 'LBLSColor';
    SetPosition(8, 128, 116, 16);
    FontDesc := '#Label1';
    Text := rsLightSquare;
    Enabled := FALSE;
  end;

  EDLSColor := TfpgEdit.Create(self);
  with EDLSColor do
  begin
    Name := 'EDLSColor';
    SetPosition(8, 146, 116, 22);
    ExtraHint := '';
    FontDesc := '#Edit1';
    Hint := rsLightSquareHint;
    ShowHint := TRUE;
    TabOrder := 5;
    Text := '00000000';
    Enabled := FALSE;
  end;

  LBDSColor := TfpgLabel.Create(self);
  with LBDSColor do
  begin
    Name := 'LBDSColor';
    SetPosition(132, 128, 116, 16);
    FontDesc := '#Label1';
    Text := rsDarkSquare;
    Enabled := FALSE;
  end;

  EDDSColor := TfpgEdit.Create(self);
  with EDDSColor do
  begin
    Name := 'EDDSColor';
    SetPosition(132, 146, 116, 22);
    FontDesc := '#Edit1';
    Hint := rsDarkSquareHint;
    ShowHint := TRUE;
    TabOrder := 6;
    Text := '00000000';
    Enabled := FALSE;
  end;
  
  LBGreen := TfpgLabel.Create(self);
  with LBGreen do
  begin
    Name := 'LBGreen';
    SetPosition(256, 128, 116, 16);
    FontDesc := '#Label1';
    Text := rsLegal;
    Enabled := FALSE;
  end;

  EDGreen := TfpgEdit.Create(self);
  with EDGreen do
  begin
    Name := 'EDGreen';
    SetPosition(256, 146, 116, 22);
    FontDesc := '#Edit1';
    Hint := rsLegalHint;
    ShowHint := TRUE;
    TabOrder := 7;
    Text := '00000000';
    Enabled := FALSE;
  end;
  
  LBRed := TfpgLabel.Create(self);
  with LBRed do
  begin
    Name := 'LBRed';
    SetPosition(380, 128, 116, 16);
    FontDesc := '#Label1';
    Text := rsCheck;
    Enabled := FALSE;
  end;

  EDRed := TfpgEdit.Create(self);
  with EDRed do
  begin
    Name := 'EDRed';
    SetPosition(380, 146, 116, 22);
    FontDesc := '#Edit1';
    Hint := rsCheckHint;
    ShowHint := TRUE;
    TabOrder := 8;
    Text := '00000000';
    Enabled := FALSE;
  end;

  LBLMColor1 := TfpgLabel.Create(self);
  with LBLMColor1 do
  begin
    Name := 'LBLMColor1';
    SetPosition(8, 176, 116, 16);
    FontDesc := '#Label1';
    Hint := '';
    Text := 'Light Marble I';
    Enabled := FALSE;
  end;

  EDLMColor1 := TfpgEdit.Create(self);
  with EDLMColor1 do
  begin
    Name := 'EDLMColor1';
    SetPosition(8, 194, 116, 22);
    ExtraHint := '';
    FontDesc := '#Edit1';
    Hint := 'First color for generation of light marble';
    ShowHint := TRUE;
    TabOrder := 9;
    Text := '00000000';
    Enabled := FALSE;
  end;

  LBLMColor2 := TfpgLabel.Create(self);
  with LBLMColor2 do
  begin
    Name := 'LBLMColor2';
    SetPosition(132, 176, 116, 16);
    FontDesc := '#Label1';
    Text := 'Light Marble II';
    Enabled := FALSE;
  end;

  EDLMColor2 := TfpgEdit.Create(self);
  with EDLMColor2 do
  begin
    Name := 'EDLMColor2';
    SetPosition(132, 194, 116, 22);
    FontDesc := '#Edit1';
    Hint := 'Second color for generation of light marble';
    ShowHint := TRUE;
    TabOrder := 10;
    Text := '00000000';
    Enabled := FALSE;
  end;
  
  LBDMColor1 := TfpgLabel.Create(self);
  with LBDMColor1 do
  begin
    Name := 'LBDMColor1';
    SetPosition(256, 176, 116, 16);
    FontDesc := '#Label1';
    Text := 'Dark Marble I';
    Enabled := FALSE;
  end;

  EDDMColor1 := TfpgEdit.Create(self);
  with EDDMColor1 do
  begin
    Name := 'EDDMColor1';
    SetPosition(256, 194, 116, 22);
    FontDesc := '#Edit1';
    Hint := 'First color for generation of dark marble';
    ShowHint := TRUE;
    TabOrder := 11;
    Text := '00000000';
    Enabled := FALSE;
  end;
  
  LBDMColor2 := TfpgLabel.Create(self);
  with LBDMColor2 do
  begin
    Name := 'LBDMColor2';
    SetPosition(380, 176, 116, 16);
    FontDesc := '#Label1';
    Text := 'Dark Marble II';
    Enabled := FALSE;
  end;

  EDDMColor2 := TfpgEdit.Create(self);
  with EDDMColor2 do
  begin
    Name := 'EDDMColor2';
    SetPosition(380, 194, 116, 22);
    FontDesc := '#Edit1';
    Hint := 'Second color for generation of dark marble';
    ShowHint := TRUE;
    TabOrder := 12;
    Text := '00000000';
    Enabled := FALSE;
  end;

  LBEngine := TfpgLabel.Create(self);
  with LBEngine do
  begin
    Name := 'LBEngine';
    SetPosition(8, 224, 488, 16);
    FontDesc := '#Label1';
    Text := rsEngine;
  end;

  EDEngine := TfpgEdit.Create(self);
  with EDEngine do
  begin
    Name := 'EDEngine';
    SetPosition(8, 242, 488, 22);
    Anchors := [anLeft, anRight, anTop];
    FontDesc := '#Edit1';
    Hint := rsEngineHint;
    ShowHint := TRUE;
    TabOrder := 13;
    Text := EmptyStr;
  end;

  LBTime := TfpgLabel.Create(self);
  with LBTime do
  begin
    Name := 'LBTime';
    SetPosition(8, 272, 116, 16);
    FontDesc := '#Label1';
    Text := rsMoveTime;
  end;

  EDTime := TfpgEdit.Create(self);
  with EDTime do
  begin
    Name := 'EDTime';
    SetPosition(8, 290, 116, 22);
    FontDesc := '#Edit1';
    Hint := rsMoveTimeHint;
    ShowHint := TRUE;
    TabOrder := 14;
    Text := '500';
  end; 
  
  CBAutoPlay := TfpgCheckBox.Create(self);
  with CBAutoPlay do
  begin
    Name := 'CBAutoPlay';
    SetPosition(132, 290, 116, 22);
    FontDesc := '#Label1';
    TabOrder := 15;
    Text := rsAutoPlay;
  end;
  
  BTSelectEngine := TfpgButton.Create(self);
  with BTSelectEngine do
  begin
    Name := 'BTSelectEngine';
    SetPosition(256, 290, 240, 24);
    Anchors := [anRight, anTop];
    Text := rsSelectEngine;
    FontDesc := '#Label1';
    TabOrder := 16;
    OnClick := @BTSelectEngineClick;
  end;
  
  LBBook := TfpgLabel.Create(self);
  with LBBook do
  begin
    Name := 'LBBook';
    SetPosition(8, 320, 488, 16);
    FontDesc := '#Label1';
    Text := rsOpeningBook;
  end;

  EDBook := TfpgEdit.Create(self);
  with EDBook do
  begin
    Name := 'EDBook';
    SetPosition(8, 338, 488, 22);
    Anchors := [anLeft, anRight, anTop];
    FontDesc := '#Edit1';
    Hint := rsOpeningBookHint;
    ShowHint := TRUE;
    TabOrder := 17;
    Text := EmptyStr;
  end;
  
  BTSelectBook := TfpgButton.Create(self);
  with BTSelectBook do
  begin
    Name := 'BTSelectBook';
    SetPosition(256, 386, 240, 24);
    Anchors := [anRight, anTop];
    Text := rsSelectBook;
    FontDesc := '#Label1';
    TabOrder := 18;
    OnClick := @BTSelectBookClick;
  end; 
  
  LBPos := TfpgLabel.Create(self);
  with LBPos do
  begin
    Name := 'LBPos';
    SetPosition(8, 418, 488, 16);
    FontDesc := '#Label1';
    Text := rsPosition;
  end;

  EDPos := TfpgEdit.Create(self);
  with EDPos do
  begin
    Name := 'EDPos';
    SetPosition(8, 436, 488, 22);
    Anchors := [anLeft, anRight, anTop];
    FontDesc := '#Edit1';
    Hint := rsPositionHint;
    ShowHint := TRUE;
    TabOrder := 19;
    Text := 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';
  end;
  
  BTStart := TfpgButton.Create(self);
  with BTStart do
  begin
    Name := 'BTStart';
    SetPosition(8, 484, 240, 24);
    Text := rsStartEschecs;
    FontDesc := '#Label1';
    Hint := rsStartEschecsHint;
    ShowHint := TRUE;
    TabOrder := 20;
    OnClick := @BTStartClick;
  end;
  
  BTQuit := TfpgButton.Create(self);
  with BTQuit do
  begin
    Name := 'BTQuit';
    SetPosition(256, 484, 240, 24);
    Anchors := [anRight, anTop];
    Text := rsQuit;
    FontDesc := '#Label1';
    TabOrder := 21;
    OnClick := @BTQuitClick;
  end;
  
  PopulateFont;
  PopulateStyle;
  PopulateLang;
  
  LoadSettings(
    LCurrPos,
    LAuto,
    LUpsideDown,
    LStyle,
    LMoveHist,
    LPosIndex,
    LEngine,
    LBook,
    LLSColor,
    LDSColor,
    LGreen,
    LRed, LLMColor1, LLMColor2, LDMColor1, LDMColor2,
    LMoveTime,
    LFont,
    LLang,
    LScale,
    LChess960
  );
  
  for i := 0 to Pred(CBFont.Items.Count) do
    if UpperCase(CBFont.Items[i]) = UpperCase(LFont) then
    begin
      CBFont.FocusItem := i;
      CBFontChange(nil);
    end;
  
  for i := 0 to Pred(CBSize.Items.Count) do
    if CBSize.Items[i] = IntToStr(LScale) then
      CBSize.FocusItem := i;
  
  CBStyle.FocusItem := Ord(LStyle);
  EDEngine.Text := LEngine;
  EDBook.Text := LBook;
  EDLSColor.Text := BGRAToStr(LLSColor);
  EDDSColor.Text := BGRAToStr(LDSColor);
  EDGreen.Text := BGRAToStr(LGreen);
  EDRed.Text := BGRAToStr(LRed);
  EDLMColor1.Text := BGRAToStr(LLMColor1);
  EDLMColor2.Text := BGRAToStr(LLMColor2);
  EDDMColor1.Text := BGRAToStr(LDMColor1);
  EDDMColor2.Text := BGRAToStr(LDMColor2);
  CBLang.FocusItem := Ord(LLang);
  EDTime.Text := IntToStr(LMoveTime);
  EDPos.Text := LCurrPos;
end;

procedure TMainForm.ItemQuitClicked(Sender: TObject);
begin
  Close;
end;

end.
