
program Assistant;

uses
{$IFDEF UNIX}
  CThreads,
{$ENDIF}
  SysUtils,
  Classes,
  
  fpg_main,
  fpg_stylemanager,
  
  MainForm,
  FormStyle;

{$I icon.inc}
{$IFDEF WINDOWS}
{$R eschecs.res}
{$ENDIF}

var
  LForm: TMainForm;
  
begin
  fpgApplication.Initialize;
  
  fpgImages.AddMaskedBMP('vfd.eschecs', @vfd_eschecs, SizeOf(vfd_eschecs), 0, 0);
  
  if fpgStyleManager.SetStyle('eschecs') then
    fpgStyle := fpgStyleManager.Style;
  
  LForm := TMainForm.Create(nil);
  try
    LForm.Show;
    fpgApplication.Run;
  finally
    LForm.Free;
  end;
end.
