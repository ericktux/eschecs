
unit Constants;

{
  Document de référence :
  fpGUI/docs/translating_your_application.txt
  
  Voir aussi :
  fpGUI/languages/README.txt
  
  Commandes utiles :
  rstconv -i units/constants.rsj -o assistant.en.po
}

interface

resourcestring
  rsUciChessEngine = 'UCI Chess Engine';
  rsPolyglotBook = 'Polyglot Book';
  rsWoodChessboardNotAvailable = 'Wood chessboard not available for size greater than 80.';
  rsQuit = 'Quit';
  rsPiecesSet = 'Pieces Set';
  rsPiecesSize = 'Pieces Size';
  rsUpsideDown = 'Upside Down';
  rsChessboardStyle = 'Chessboard Style';
  rsLanguage = 'Language';
  rsLightSquare ='Light Square';
  rsLightSquareHint = 'Light squares color (for ''simple'' style)';
  rsDarkSquare = 'Dark Square';
  rsDarkSquareHint = 'Dark squares color (for ''simple'' style)';
  rsLegal = 'Legal';
  rsLegalHint = 'Legal target squares color';
  rsCheck = 'Check';
  rsCheckHint = 'King in check square color';
  rsEngine = 'Engine';
  rsEngineHint = 'Engine path';
  rsMoveTime = 'Move Time';
  rsMoveTimeHint = 'Time available (in milliseconds) for one computer move';
  rsAutoPlay = 'Auto Play';
  rsSelectEngine = 'Select Engine';
  rsOpeningBook = 'Opening Book';
  rsOpeningBookHint = 'Polyglot opening book';
  rsSelectBook = 'Select Book';
  rsPosition = 'Position';
  rsPositionHint = 'Start position, in FEN format';
  rsStartEschecs = 'Start Eschecs';
  rsStartEschecsHint = 'Start Eschecs and close this application';
  
implementation

end.
