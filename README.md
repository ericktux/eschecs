
# Eschecs

## Overview

*Eschecs* is an application to play chess against [UCI](http://www.shredderchess.com/chess-info/features/uci-universal-chess-interface.html) engines.

![alt text](images/screenshots/eschecs512.png)

## Usage

*Eschecs* is shipped with an engine ([Cheng](http://vlasak.biz/cheng/)).

If you wish to use another one, start *Eschecs* with the engine path as parameter. For example (under Linux) :

```
./eschecs /home/roland/chess/engines/CT800_V1.42
```

You can find [here](https://gitlab.com/rchastain/eschecs/-/blob/master/ENGINES.md) a list of UCI engines that you could use with *Eschecs*.

## Keyboard Controls

| Key | Effect |
| --- | --- |
| ↑ | Go to last position |
| ↓ | Go back to first position |
| ← | Go back to previous position |
| → | Go to next position |
| D | Open debug window |
| Ctrl+Q | Quit |

When you quit the program by pressing Ctrl+Q, the current game and options are automatically saved. To quit the program without saving, use the window *Close* button.  

## Book

*Eschecs* uses a Polyglot opening book.

The default book is the [small Polyglot book](http://www.open-aurec.com/chesswar/download.html) by Olivier Deville.

If you wish to use another one, please open *config/eschecs.ini* in a text editor and change this line:

```
book=books/gm2001.bin
```

## Command line options

The behaviour and the appearance of *Eschecs* can be customized using command line parameters. Please see [available options](https://gitlab.com/rchastain/eschecs/-/blob/master/OPTIONS.md).

## Build

You can build *Eschecs* by yourself, provided that you have the Free Pascal compiler installed. If it will be easier if you also have *git*.

Please see [instructions](https://gitlab.com/rchastain/eschecs/-/blob/master/BUILD.md).

## Authors

*Eschecs* is a program written by Roland Chastain, with contributions by [Johann Elsass](https://github.com/circular17) and [Fred van Stappen](https://github.com/fredvs).

*Cheng* is a chess engine written by Martin Sedlák.
