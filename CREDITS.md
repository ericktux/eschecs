
# Credits

## Graphics

The application icon is the white king of the [Chess Montreal font](http://alcor.concordia.ca/~gpkatch/montreal_font.html).

The wood chessboard and its pieces are the work of [Daniela Di Lena](https://dilena.de/chess-artwork-pieces-and-board-art-assets).

The other pieces set has been made from TrueType chess fonts:

* *Chess Alpha* by Eric Bentzen
* *Chess Condal, Chess Line, Chess Mark* by Armando Marroquin
* *Chess Montreal* by Gary Katch

## Sounds

The sound effects come from [The Essential Retro Video Game Sound Effects Collection][1] by Juhani Junkala.

[1]: https://opengameart.org/content/512-sound-effects-8-bit-style

## Translations

Thanks to the translators:

* Martin Sedlák (Czech)
* Jean-Luc Gofflot (Dutch)
* Users of the [German Lazarus forum](https://www.lazarusforum.de/index.php) (German)
* Marcello Basso (Italian)
* Ñuño Martínez (Spanish)
* Victor Morev (Russian)
