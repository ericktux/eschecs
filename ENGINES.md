
# Engines

| Engine | Version | Logo | Author | Protocol | Code source | FRC support | Link |
| --- | --- | --- | --- | --- | --- | --- | --- |
| Alouette | 0.1.6 | ![alt text](images/logos/alouette/logo.png) | Roland Chastain | UCI | Pascal | Yes | [Website](https://gitlab.com/rchastain/alouette) |
| Cheese | 3.1.1 | ![alt text](images/logos/cheese/cheese.png) | Patrice Duhamel | UCI | C++ | Yes | [Website](http://cheesechess.free.fr/en/index.html) |
| Cheng4 | 0.41 | ![alt text](images/logos/cheng/logo.png) | Martin Sedlak | UCI | C++ | Yes | [Website](http://www.vlasak.biz/cheng/) |
| CT800 | 1.42 | ![alt text](images/logos/ct800/CT800_V1.34_x32.png) | Rasmus Althoff | UCI | C | No | [Website](https://www.ct800.net/) |
| Enxadrista | 1.01 | ![alt text](images/logos/enxadrista/enxadrista.gif) | Alcides Schulz | XBoard | C# | No | [Website](https://github.com/alcides-schulz/Xadrez) |
| Floyd | 0.9 | ![alt text](images/logos/floyd/floyd-logo.png) | Marcel van Kervinck | UCI | C | No | [Website](https://marcelk.net/floyd/) [Website](https://github.com/kervinck/floyd) |
| Fridolin | 3.10 | ![alt text](images/logos/fridolin/Fridolin.jpg) | Christian Sommerfeld | UCI | C++ | Yes | [Website](https://sites.google.com/site/fridolinchess/) |
| Fruit | 2.1 | ![alt text](images/logos/fruit/fruit-logo_100x50.jpg) | Fabien Letouzey | UCI | C++ | No | [Website](https://www.fruitchess.com) |
| Galjoen | 0.40 | ![alt text](images/logos/galjoen/galjoen_100x50.png) | Werner Taelemans | UCI | C++ | Yes | [Website](http://www.goudengaljoen.be/) |
| Geko | 0.4.3 | ![alt text](images/logos/geko/geko_043.png) | Giuseppe Cannella | XBoard | - | No | [Website](https://github.com/gekomad) |
| Hermann | 2.8 | ![alt text](images/logos/hermann/hermann-2.jpg) | Volker Annuss | UCI | - | Yes | [Website](http://www.nnuss.de/Hermann/) |
| La Dame Blanche | 2.0 | ![alt text](images/logos/ladameblanche/ladameblanche.gif) | Marc-Philippe Huget | XBoard | - | No | [Website](http://www.quarkchess.de/ladameblanche/) |
| LittleWing | 0.6 | ![alt text](images/logos/littlewing/littlewing.gif) | Vincent Ollivier | UCI | Rust | No | [Website](https://vinc.cc/projects/littlewing/) [Website](https://github.com/vinc/littlewing) |
| Monolith | 1.0 | ![alt text](images/logos/monolith/Monolith_04.png) | Jonas Mayr | UCI | C++ | Yes | [Website](https://github.com/cimarronOST/Monolith) |
| Moustique | 0.4.1 | ![alt text](images/logos/moustique/Farman-F455-Moustique.png) | Jürgen Schlottke, Roland Chastain | UCI | Pascal | No | [Website](https://gitlab.com/rchastain/moustique) |
| N.E.G. | 1.2 | ![alt text](images/logos/neg/neg.gif) | Harm Geert Muller | XBoard | C | No | [Website](https://home.hccnet.nl/h.g.muller/chess.html) |
| Pharaon | 3.5.1 | ![alt text](images/logos/pharaon/Pharaon3.png) | Frank Zibi | UCI | - | Yes | [Website](http://www.fzibi.com/pharaon.htm) |
| PolarChess |  | ![alt text](images/logos/polarchess/polarchess.gif) | Odd Gunnar Malin | UCI | C++ | Yes | [Website](https://github.com/OGMalin/Polarchess) |
| Rodent |  | ![alt text](images/logos/rodent/rodent2.jpg) | Pawel Koziol | UCI | C++ | No | [Website](http://www.pkoziol.cal24.pl/rodent/rodent.htm) |
| Sapeli | 1.53 | ![alt text](images/logos/sapeli/logo.jpg) | Toni Helminen | UCI | C | Yes | [Website](https://github.com/SamuraiDangyo/Sapeli) |
| Senpai | 2.0 | ![alt text](images/logos/senpai/senpai2_2_102.png) | Fabien Letouzey | UCI | C++ | Yes | [Website](http://www.amateurschach.de/main/_senpai.htm) |
| SlowChess |  | ![alt text](images/logos/slowchess/slow.png) | Jonathan Kreuzer | UCI | C++ | Yes | [Website](https://www.3dkingdoms.com/chess/slow.htm) |
| Xadreco | 5.71 | ![alt text](images/logos/xadreco/xadreco-logo1.jpg) | Ruben Carlo Benante | XBoard | C | No | [Website](https://github.com/drbeco/xadreco) |

For more information, please see [CCRL website](https://www.computerchess.org.uk/ccrl/).
