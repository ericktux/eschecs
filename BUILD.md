
# Build

To build *Eschecs*, you need the Free Pascal compiler, and it will easier if you also have *git*. Otherwise you will have to download separately the libraries used by *Eschecs* (1).

In the following instructions I suppose that you have *git* installed.

## Download

Download the source code of the project, including the libraries, by using the following command:

```
git clone --recurse-submodules https://gitlab.com/rchastain/eschecs
```

## Build using *make*

Then build *Eschecs* as follows:

```
cd eschecs/source
make
```

Then build *Assistant* (2):

```
cd assistant
make
```

Then go to the main directory and start *Assistant*:

```
cd ../..
./assistant
```

Or start *Eschecs* directly, if you don't need to change options:

```
./eschecs
```

Please notice that the *Makefile* included in this repository have been made for Linux. It you try to compile on another OS, you could have to retouch it.

## Build using Lazarus or MSEide

If you prefer to build *Eschecs* using your favourite IDE, you will find in *eschecs/source* a Lazarus project (*eschecs.lpi*) and a MSEide project (*eschecs.prj*).  
  
  
---

(1) *Eschecs* uses the following libraries.

* [fpGUI](https://github.com/graemeg/fpGUI)
* [BGRABitmap](https://github.com/bgrabitmap/bgrabitmap)
* [LazUtils](https://sourceforge.net/projects/lazarus/)
* [uos](https://github.com/fredvs/uos)

(2) *Assistant* is an application for setting *Eschecs* options without using command line.
