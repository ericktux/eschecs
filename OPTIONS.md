# Command line options

The behaviour and the appearance of *Eschecs* can be customized using the following command line parameters.

| Parameter name | Parameter value |
| --- | --- |
| `-o <v>`, `--openingbook=<v>` | Polyglot opening book. |
| `-p <v>`, `--position=<v>` | The position to be loaded, in FEN format. |
| `-a <v>`, `--autoplay=<v>` | The computer will be the second player. |
| `-t <v>`, `--time=<v>` | Time allowed for the computer move, in milliseconds. |
| `-u <v>`, `--upsidedown=<v>` | Draw the chessboard upside down. |
| `-c <v>`, `--chessboard=<v>` | Appearance of the chessboard. |
| `-m <v>`, `--marblecolors=<v>` | Marble colors. |
| `-f <v>`, `--font=<v>` | Piece set. See below possible values. |
| `-l <v>`, `--language=<v>` | Language. See below possible values. |
| `-s <v>`, `--size=<v>` | Size of the square. See below possible values. |
| `-w <v>`, `--white=<v>` | Color of white squares. |
| `-b <v>`, `--black=<v>` | Color of black squares, same format. |
| `-g <v>`, `--green=<v>` | Color for legal target squares. |
| `-r <v>`, `--red=<v>` | Color for the square of a king being in check. |

See *eschecs.sh* (or *eschecs.cmd*, if you are under Windows) for examples.

See below available values for `font`, `size` and `language` parameters.

## Available fonts and sizes

```
Alpha    30, 40, 50, 60, 70, 80, 90, 100
Condal   30, 40, 50, 60, 70, 80, 90, 100
Line     30, 40, 50, 60, 70, 80, 90, 100
Lucena   30, 40, 50, 60, 70, 80, 90, 100
Magnetic 30, 40, 50, 60, 70, 80, 90, 100
Mark     30, 40, 50, 60, 70, 80, 90, 100
Montreal 30, 40, 50, 60, 70, 80, 90, 100
Usual    30, 40, 50, 60, 70, 80, 90, 100
Wood     30, 40, 50, 60, 70, 80
```

You can see screenshots [here](https://gitlab.com/rchastain/eschecs/-/blob/master/images/screenshots/README.md).

## Available languages

* Czech
* Dutch
* English
* French
* German
* Italian
* Russian
* Spanish
