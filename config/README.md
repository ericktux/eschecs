# Configuration files

The program always loads engines list from *engines.ini*.

To install new engines on Linux (for example), the better way is to edit *linux.ini* and then overwrite *engines.ini*.

```
cp -f linux.ini engines.ini
```

When you edit the INI file, you don't need to redo by hand the engines numerotation (**[engine0]**, **[engine1]**, etc.). If there are holes or repetitions in the numerotation, you can use the script *sed.lua* to redo the numerotation.

```
lua linux.ini linux.ini
```

Or, if you prefer to check the result before modifying your file:

```
lua linux.ini result.txt
```

The value of the **directory** key can be either an absolute path or a path relative to the *eschecs* executable.

For now, the value of the **protocol** key is useless, since *Eschecs* supports only UCI engines.
