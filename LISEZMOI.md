
# Eschecs

## Brève description

*Eschecs* est une application gratuite et multiplateforme permettant de jouer aux échecs contre des automates UCI (pour *[Universal Chess Interface](http://www.shredderchess.com/chess-info/features/uci-universal-chess-interface.html)*).

![alt text](images/screenshots/eschecs512.png)

## Mode d'emploi

*Eschecs* est livré avec un automate ([Cheng](http://vlasak.biz/cheng/)).

Si vous souhaitez jouer contre un autre automate, lancez *Eschecs* avec le chemin de l'automate comme paramètre. Par exemple (sous Linux) :

```
./eschecs /home/roland/chess/engines/CT800_V1.42
```

Vous pouvez trouver sur [cette page](https://gitlab.com/rchastain/eschecs/-/blob/master/ENGINES.md) une liste d'automates.

## Clavier

| Touche | Effet |
| --- | --- |
| ↑ | Aller à la dernière position |
| ↓ | Revenir à la position de départ |
| ← | Revenir à la position précédente |
| → | Aller à la position suivante |
| D | Ouvrir la fenêtre de débogage |
| Ctrl+Q | Quitter l'application |

Lorsque vous quittez le programme en appuyant sur Ctrl+Q (ou sur l'item de menu correspondant), la partie et les options sont automatiquement enregistrées. Pour fermer l'application sans enregistrer la partie, appuyez sur le bouton *Fermer* de la fenêtre.  

## Livre

*Eschecs* utilise un livre d'ouvertures au format Polyglot.

Le livre par défaut est le [petit livre Polyglot](http://www.open-aurec.com/chesswar/download.html) d'Olivier Deville.

Si vous souhaitez utiliser un autre livre, ouvrez le fichier *config/eschecs.ini* dans un éditeur de texte et modifiez la ligne suivante :

```
book=books/gm2001.bin
```

Vous pouvez également sélectionner un livre d'ouvertures au moyen de l'option **--openingbook** (voir section suivante), ou en lançant *Eschecs* à partir de l'application *Assistant*.
 
## Options de la ligne de commande

Le comportement et l'apparence d'*Eschecs* peuvent être réglés au moyen des paramètres de la ligne de commande. Voyez les différentes [options disponibles](https://gitlab.com/rchastain/eschecs/-/blob/master/OPTIONS.md).

## Compilation

Vous pouvez compiler *Eschecs* par vous-même, pourvu que le compilateur Free Pascal soit installé sur votre machine. Ce sera encore plus facile si *git* et *make* sont aussi installés.

Il est également possible de compiler *Eschecs* dans l'EDI [Lazarus](https://pascal.developpez.com/telecharger/detail/id/1657/Lazarus).

Voyez les [instructions pour la compilation](https://gitlab.com/rchastain/eschecs/-/blob/master/BUILD.md).

## Le saviez-vous ?

Le nom *Eschecs* est la vieille façon d'écrire le nom de ce jeu. On trouve ce mot, par exemple, dans le dictionnaire d'Antoine Furetière :

> ESCHECS, ou ECHECS. Jeu de petites pièces de bois tourné, qui servent à jouer sur un tablier ou damier divisé en 64 carreaux, où l'adresse est tellement requise, que le hasard ne s'en mêle point, & on ne perd que par sa faute. Il y a de chaque côté huit pièces & huit pions, qui ont divers mouvemens et règles pour marcher. La commune opinion des Anciens est que ce fut Palamède qui inventa les échecs & l'échiquier pendant le siège de Troye. D'autres l'attribuent à un Diomède qui vivait sous Alexandre. Mais la vérité est que ce jeu est si ancien, qu'on n'en peut sçavoir l'auteur.

## Auteurs

*Eschecs* est un programme écrit en Pascal par Roland Chastain, avec des contributions de [Johann Elsass](https://github.com/circular17) (qui est l'auteur de l'excellente bibliothèque [BGRABitmap](https://lazarus.developpez.com/telecharger/detail/id/433/Bibliotheque-BGRABitmap)) et de [Fred van Stappen](https://github.com/fredvs).

*Cheng* est un programme de Martin Sedlák.
